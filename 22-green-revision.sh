#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

service="tada"
namespace="demo"

read -d '' CODE << EOF
function main(params) {
  return {
    message: "🎉🎉🎉 tada! Hello " + params.getString("name"),
    revision: "🟢"
  }
}
EOF

# 👋 create a green revision with 0% traffic 
# 👋 keep the blue revision with 100% traffic 

kn service update ${service} \
  --namespace ${namespace} \
  --env FUNCTION_CODE="$CODE" \
  --revision-name green \
  --traffic ${service}-blue=100 \
  --traffic ${service}-green=0

kn revision list -s ${service} -n ${namespace}
echo ""
kn route describe ${service} -n ${namespace}


