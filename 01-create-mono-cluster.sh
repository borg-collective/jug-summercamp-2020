#!/bin/bash
# check if env is ok first
kn version
RET=$?
if [ "x$RET" != "x0" ]
then
  echo "Install kn first"
  echo " see https://github.com/knative/client"
  echo " and https://knative.dev/docs/install/install-kn/"
  exit $RET
fi

which kubectl
RET=$?
if [ "x$RET" != "x0" ]
then
  echo "Install kubectl"
  echo "> For linux"
  echo "curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
  echo "chmod 775 kubectl && sudo mv kubectl /usr/local/bin/kubectl"
  echo "> For mac"
  echo "brew install kubectl"

  exit $RET
fi

# Read configuration
eval $(cat vm.config)

# Create Ubuntu VM
multipass launch --name ${vm_name} --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
  --cloud-init ./cloud-init.yaml

if [ ! -d "config" ];then
  mkdir config
fi


multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
curl -sfL https://get.k3s.io | sh -s - --disable traefik
#curl -sfL https://get.k3s.io | sh -
EOF

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
echo "😃 📦 K3s initialized on ${vm_name} ✅"
echo "🖥 IP: ${IP}"

# Generate config file for kubectl
# and copy it to /config
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
sed "s/127.0.0.1/$IP/" /etc/rancher/k3s/k3s.yaml  > /tmp/k3s.yaml
chown ubuntu /tmp/k3s.yaml

kubectl wait --for=condition=available deployment/coredns -n kube-system 
kubectl wait --for=condition=available deployment/local-path-provisioner -n kube-system  
kubectl wait --for=condition=available deployment/metrics-server -n kube-system  
echo "🎉 K3S installation complete 🍾"
EOF

multipass transfer ${vm_name}:/tmp/k3s.yaml config/k3s.yaml