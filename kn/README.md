# kn

- https://github.com/knative/client
- https://knative.dev/docs/install/install-kn/

# Linux

```bash
curl  https://storage.googleapis.com/knative-nightly/client/latest/kn-linux-amd64 -o kn
sudo mv kn /usr/local/bin
sudo chmod 775 /usr/local/bin/kn
```

# Windows
https://storage.googleapis.com/knative-nightly/client/latest/kn-windows-amd64.exe
...

# Mac
https://storage.googleapis.com/knative-nightly/client/latest/kn-darwin-amd64

Ou
```bash
brew tap knative/client
brew install kn
```