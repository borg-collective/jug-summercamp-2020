#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

service="hello-js"
namespace="demo"

read -d '' CODE << EOF
function hello(params) {
  return {
    message: "😃 Hello World",
    total: 42,
    authors: ["@k33g_org","@_louidji"],
    params: params.getString("name")
  }
}
EOF

# create or update the service
kn service create --force ${service} \
  --namespace ${namespace} \
  --env FUNCTION_NAME="hello" \
  --env LANG="js" \
  --env FUNCTION_CODE="${CODE}" \
  --env CONTENT_TYPE="application/json;charset=UTF-8" \
  --image registry.gitlab.com/borg-collective/7of9:latest

