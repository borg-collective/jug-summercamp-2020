#!/bin/bash
. ./env.sh

# Optional Serving extensions
kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/serving-hpa.yaml
