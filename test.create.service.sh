#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

eval $(cat vm.config)
ip=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

service="coucou"
namespace="demo"

read -d '' CODE << EOF
function coucou(params) {
  return {
    message: "😃 Hello World 👋...",
    total: 42,
    authors: ["@k33g_org","@_louidji"],
    params: params.getString("name")
  }
}
EOF

# create or update the service
kn service create --force ${service} \
  --namespace ${namespace} \
  --env FUNCTION_NAME="coucou" \
  --env LANG="js" \
  --env FUNCTION_CODE="$CODE" \
  --env CONTENT_TYPE="application/json;charset=UTF-8" \
  --image registry.gitlab.com/borg-collective/7of9:latest \
  --env REDIS_HOST="redis-mother.database" \
  --env SERVICE_HOST="${service}.${namespace}.${ip}.xip.io"
