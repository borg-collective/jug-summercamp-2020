#!/bin/bash
. ./env.sh

kubectl apply -f ./traefik --wait

ip=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

kubectl wait deployment traefik-ingress-controller -n kube-system --for condition=available



cat << EOF | kubectl apply -f -
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: grafana
  namespace: knative-monitoring
  labels:
    app: grafana
spec:
  entryPoints:
    - web
  routes:
  - match: Host(\`grafana.${ip}.nip.io\`)
    kind: Rule
    services:
    - name: grafana
      port: 30802              
EOF

echo "http://grafana.${ip}.nip.io:8080"