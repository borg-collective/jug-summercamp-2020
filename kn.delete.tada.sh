#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

service="tada"
namespace="demo"

kn service delete ${service} -n ${namespace}
