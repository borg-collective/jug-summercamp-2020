#!/bin/bash
# $ source ./env.sh # load env
eval $(cat vm.config)
export vm_name=$vm_name
export vm_cpus=$vm_cpus
export vm_mem=$vm_mem
export vm_disk=$vm_disk
export knative_version=$knative_version
export KUBECONFIG=$PWD/config/k3s.yaml