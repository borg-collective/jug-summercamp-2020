#!/bin/bash
. ./env.sh

# https://knative.dev/docs/install/any-kubernetes-cluster/


# ===========================================================
# Configure DNS
# -------------
# Magic DNS (xip.io)
# We ship a simple Kubernetes Job called “default domain” 
# that will configure Knative Serving to use xip.io 
# as the default DNS suffix.
# ===========================================================

kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-default-domain.yaml" --wait=true

# ====== wait ... ======
kubectl wait --for=condition=complete job/default-domain -n knative-serving

kubectl get pods --namespace knative-serving
