#!/bin/bash
. ./env.sh
# Refs.
# https://knative.dev/docs/install/any-kubernetes-cluster/
# https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/


# Install Knative Serving

# ===========================================================
# Install the Custom Resource Definitions
# ---------------------------------------
# Custom resources are extensions of the Kubernetes API.
# ===========================================================

kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-crds.yaml" --wait=true

# ===========================================================
# Install the core components of Serving
# ===========================================================

kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-core.yaml" --wait=true


# ===========================================================
# Install a networking layer
# --------------------------
# Install and configure Kourier:
#
# Kourier is an Ingress for Knative Serving. 
# Kourier is a lightweight alternative for the Istio ingress
# Réf. https://github.com/knative/net-kourier
# ===========================================================

kubectl apply --filename https://raw.githubusercontent.com/knative/serving/v${knative_version}/third_party/kourier-latest/kourier.yaml --wait=true
kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress.class":"kourier.ingress.networking.knative.dev"}}'


# ====== wait ... ======
kubectl wait --for=condition=available deployment/3scale-kourier-control -n knative-serving
kubectl wait --for=condition=available deployment/3scale-kourier-gateway -n kourier-system 
kubectl wait --for=condition=Ready pod -l app=svclb-kourier -n kourier-system

kubectl wait --for=condition=available deployment/activator -n knative-serving 
kubectl wait --for=condition=available deployment/autoscaler -n knative-serving 
kubectl wait --for=condition=available deployment/controller -n knative-serving
kubectl wait --for=condition=available deployment/webhook -n knative-serving

kubectl --namespace kourier-system get service kourier
