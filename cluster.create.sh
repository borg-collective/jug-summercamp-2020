#!/bin/bash

# check if env is ok first
kn version
RET=$?
if [ "x$RET" != "x0" ]
then
  echo "Install kn first"
  echo " see https://github.com/knative/client"
  echo " and https://knative.dev/docs/install/install-kn/"
  exit $RET
fi

which kubectl
RET=$?
if [ "x$RET" != "x0" ]
then
  echo "Install kubectl"
  echo "> For linux"
  echo "curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
  echo "chmod 775 kubectl && sudo mv kubectl /usr/local/bin/kubectl"
  echo "> For mac"
  echo "brew install kubectl"

  exit $RET
fi

# Read configuration
eval $(cat vm.config)

# Create Ubuntu VM
multipass launch --name ${vm_name} --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
  --cloud-init ./cloud-init.yaml

if [ ! -d "config" ];then
  mkdir config
fi

# Install K3s
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
curl -sfL https://get.k3s.io | sh -s - --disable traefik
#curl -sfL https://get.k3s.io | sh -
EOF

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
echo "😃 📦 K3s initialized on ${vm_name} ✅"
echo "🖥 IP: ${IP}"

# Generate config file for kubectl
# and copy it to /config
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
sed "s/127.0.0.1/$IP/" /etc/rancher/k3s/k3s.yaml  > /tmp/k3s.yaml
chown ubuntu /tmp/k3s.yaml

kubectl wait --for=condition=available deployment/coredns -n kube-system 
kubectl wait --for=condition=available deployment/local-path-provisioner -n kube-system  
kubectl wait --for=condition=available deployment/metrics-server -n kube-system  
echo "🎉 K3S installation complete 🍾"
EOF

multipass transfer ${vm_name}:/tmp/k3s.yaml config/k3s.yaml

# Refs.
# https://knative.dev/docs/install/any-kubernetes-cluster/
# https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
# Since Knative has its own network layer, we need to disable k3s' Traefik during its installation
# to make sure Kourier proxy gets a LoadBalancer IP
#curl -sfL https://get.k3s.io | sh -s - --disable traefik

# Install Knative Serving

# ===========================================================
# Install the Custom Resource Definitions
# ---------------------------------------
# Custom resources are extensions of the Kubernetes API.
# ===========================================================

kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-crds.yaml"

# ===========================================================
# Install the core components of Serving
# ===========================================================

kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-core.yaml"


# ===========================================================
# Install a networking layer
# --------------------------
# Install and configure Kourier:
#
# Kourier is an Ingress for Knative Serving. 
# Kourier is a lightweight alternative for the Istio ingress
# Réf. https://github.com/knative/net-kourier
# ===========================================================

kubectl apply --filename https://raw.githubusercontent.com/knative/serving/v${knative_version}/third_party/kourier-latest/kourier.yaml
kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress.class":"kourier.ingress.networking.knative.dev"}}'


# ====== wait ... ======
# kubectl wait --for=condition=available deployment/3scale-kourier-control -n kourier-system
# kubectl wait --for=condition=available deployment/svclb-kourier -n kourier-system 
kubectl wait --for=condition=available deployment/3scale-kourier-gateway -n kourier-system 

kubectl wait --for=condition=available deployment/activator -n knative-serving 
kubectl wait --for=condition=available deployment/autoscaler -n knative-serving 
kubectl wait --for=condition=available deployment/controller -n knative-serving
kubectl wait --for=condition=available deployment/webhook -n knative-serving

kubectl --namespace kourier-system get service kourier
EOF

# https://knative.dev/docs/install/any-kubernetes-cluster/
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF

# ===========================================================
# Configure DNS
# -------------
# Magic DNS (xip.io)
# We ship a simple Kubernetes Job called “default domain” 
# that will configure Knative Serving to use xip.io 
# as the default DNS suffix.
# ===========================================================

kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-default-domain.yaml"

# ====== wait ... ======
#kubectl wait --for=condition=available deployment/default-domain -n knative-serving

kubectl get pods --namespace knative-serving
EOF

# Optional Serving extensions
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
# ===========================================================
# Install HPA autoscalling
# ---------------------------------------
# Knative also supports the use of the Kubernetes Horizontal 
# Pod Autoscaler (HPA) for driving autoscaling decisions. 
# The following command will install the components needed 
# to support HPA-class autoscaling:
# ===========================================================

kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/serving-hpa.yaml
EOF

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
# ===========================================================
# Installing the Observability plugin
# ---------------------------------------
# Install the following observability features to enable 
# logging, metrics, and request tracing in your Serving and 
# Eventing components.
# ===========================================================

# avoid error: (I don't know why)
kubectl create namespace istio-system

# ===========================================================
# Oservibility plugins require that 
# you first install the core:
# ===========================================================

kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/monitoring-core.yaml

# ===========================================================
# Install Prometheus and Grafana for metrics:
# ===========================================================

kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/monitoring-metrics-prometheus.yaml

EOF

. ./env.sh
namespace="demo"
kubectl create namespace ${namespace}

db_namespace="database"
kubectl create namespace ${db_namespace}
# PersistentVolumeClaim
kubectl apply -f redis.pvc.yaml -n ${db_namespace}
# Install Redis
kubectl apply -f redis.yaml -n ${db_namespace}